quiz_answer = {
    "q1": "Siapakah yang mencipta Python?",
    "a1": [
        [{"text": "Guido van Rossum", "callback_data": "q1-c"}],
        [
            {"text": "Google", "callback_data": "q1-w"},
            {"text": "Matz", "callback_data": "q1-w"},
        ],
        [{"text": "Dennis Ritchie", "callback_data": "q1-w"}],
    ],
    "q2": "Apakah laman web rasmi Python?",
    "a2": [
        [
            {"text": "python.com", "callback_data": "q2-w"},
            {"text": "python.net", "callback_data": "q2-w"},
        ],
        [
            {"text": "python.org", "callback_data": "q2-c"},
            {"text": "python-lang.com", "callback_data": "q2-w"},
        ],
    ],
    "q3": "Nyatakan versi Python yang terkini",
    "a3": [
        [
            {"text": "2.7", "callback_data": "q3-w"},
            {"text": "3.5", "callback_data": "q3-w"},
            {"text": "3.6", "callback_data": "q3-w"},
            {"text": "3.7", "callback_data": "q3-c"},
        ]
    ],
    "q4": "Yang manakah antara berikut bukan libraries dalam python",
    "a4": [
        [
            {"text": "requests", "callback_data": "q4-w"},
            {"text": "django", "callback_data": "q4-w"},
        ],
        [{"text": "laravel", "callback_data": "q4-c"}],
    ],
}

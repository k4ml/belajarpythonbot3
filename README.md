# belajarpythonbot3

- the live version of this bot is at [@BelajarPythonBot](http://t.me/belajarpythonbot) telegram

## Credit

- built using flask, zappa

## Configs

- `config_eg.py` is an example file, please rename it to `config.py` and fill in the appropriate information

## Development

- to run locally, install `ngrok` and run `ngrok http 5000`
- run flask `env FLASK_APP=server.py FLASK_DEBUG=1 flask run`
- set your telegram bot's [webhook](https://core.telegram.org/bots/api#setwebhook) to your ngrok link

## Deploy

- set your zappa with `zappa init`, then `zappa deploy dev`
- set your telegram bot's [webhook](https://core.telegram.org/bots/api#setwebhook) to your live api endpoint
